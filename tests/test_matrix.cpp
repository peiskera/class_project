#include <iostream>
#include <cstdlib>
#include <vector>
#include <cassert>
#include "matrix.h"
//#include "matrix.cc"

//function declarations
void test_matrix_construction();
void test_matrix_set();
void test_matrix_setElement();
void test_matrix_getRows();
void test_matrix_getCols();
void test_matrix_getElement();
void test_matrix_addMatrix();
void test_matrix_addScalar();
void test_matrix_multiplyMatrix();
void test_matrix_transpose();

int main()
{
  //call all of the test functions
  test_matrix_construction();
  test_matrix_set();
  test_matrix_setElement();
  test_matrix_getRows();
  test_matrix_getCols();
  test_matrix_getElement();
  test_matrix_addMatrix();
  test_matrix_addScalar();
  test_matrix_multiplyMatrix();
  test_matrix_transpose();

  return 0;
}

void test_matrix_construction()
{
  Matrix A = Matrix(3,3,2);
  for(int i=0; i<A.getRows(); i++)
  {
    for(int j=0; j<A.getCols(); j++)
    {
      assert(A.getElement(i,j)==2);
    }
  }
}

void test_matrix_set()
{
  Matrix A = Matrix(3,3,2);
  A.set(5);
  for(int i=0; i<A.getRows(); i++)
  {
    for(int j=0; j<A.getCols(); j++)
    {
      assert(A.getElement(i,j)==5);
    }
  }
}

void test_matrix_setElement()
{
  Matrix A = Matrix(3,3,2);
  A.setElement(2,2,6);
  assert(A.getElement(2,2)==6);
}

void test_matrix_getRows()
{
  Matrix A = Matrix(3,4,2);
  assert(A.getRows()==3);
}

void test_matrix_getCols()
{
  Matrix A = Matrix(3,4,2);
  assert(A.getCols()==4);
}

void test_matrix_getElement()
{
  Matrix A = Matrix(4,3,0);
  for(int i=0; i<A.getRows(); i++)
  {
    for(int j=0; j<A.getCols(); j++)
    {
      A.setElement(i,j,i+j);
    }
  }
  assert(A.getElement(2,2)==4);
}

void test_matrix_addMatrix()
{
  Matrix A = Matrix(3,3,2);
  Matrix B = Matrix(3,3,3);
  Matrix C = A + B;
  for(int i=0; i<C.getRows(); i++)
  {
    for(int j=0; j<C.getCols(); j++)
    {
      assert(C.getElement(i,j)==5);
    }
  }
}

void test_matrix_addScalar()
{
  Matrix A = Matrix(3,3,2);
  Matrix B = A + 3;
  for(int i=0; i<B.getRows(); i++)
  {
    for(int j=0; j<B.getCols(); j++)
    {
      assert(B.getElement(i,j)==5);
    }
  }
}

void test_matrix_multiplyMatrix()
{
  Matrix A = Matrix(2,2,2);
  Matrix B = Matrix(2,2,1);
  B.setElement(0,1,2);
  B.setElement(1,0,3);
  B.setElement(1,1,4);
  Matrix C = A * B;
  assert(C.getElement(0,0)==8);
  assert(C.getElement(0,1)==12);
  assert(C.getElement(1,0)==8);
  assert(C.getElement(1,1)==12);
}

void test_matrix_transpose()
{
  Matrix A = Matrix(2,2,1);
  A.setElement(0,1,2);
  A.setElement(1,0,3);
  A.setElement(1,1,4);
  Matrix B = A.transpose();
  assert(B.getElement(0,0)==1);
  assert(B.getElement(0,1)==3);
  assert(B.getElement(1,0)==2);
  assert(B.getElement(1,1)==4);
}
