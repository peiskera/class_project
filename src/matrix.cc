#include "matrix.h"

//function to generate a random number
double Random()
{
  return (double)(rand() % 20);
}

//default constructor creates a 2x2 matrix where all elements are 1
Matrix::Matrix()
{
    n_rows = 2;
    n_cols = 2;
    matrix.resize(n_rows);
    for(int i=0; i<matrix.size(); i++)
    {
        matrix[i].resize(n_cols, 1);
    }
}

//copy constructor creates a matrix identical to an existing one
Matrix::Matrix(const Matrix &A)
{
  n_rows = A.getRows();
  n_cols = A.getCols();
  matrix = A.matrix;
}

//this constructor allows the user to define the size of the matrix and what number to fill it with
Matrix::Matrix(int nrows, int ncols, double init)
{
  n_rows = nrows;
  n_cols = ncols;
  matrix.resize(nrows);
  for(int i=0; i<matrix.size(); i++)
  {
    matrix[i].resize(ncols, init);
  }
}

//destructor
Matrix::~Matrix()
{
}

//this method returns the number of rows in the matrix
int Matrix::getRows() const
{
  return n_rows;
}

//this method returns the number of columns in the matrix
int Matrix::getCols() const
{
  return n_cols;
}

//this method returns a given element of the matrix
double Matrix::getElement(const int &rownum, const int &colnum) const
{
  return matrix[rownum][colnum];
}

//this method sets all elements in the matrix to be the given value
void Matrix::set(const double &num)
{
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      matrix[i][j] = num;
    }
  }
}

//this method sets all elements in the matrix to be different random values
void Matrix::setRandom()
{
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      matrix[i][j] = Random();
    }
  }
}

//this method sets a given element of the matrix to be the given value
void Matrix::setElement(const int &rownum, const int &colnum, const double &num)
{
  matrix[rownum][colnum] = num;
}

//this method prints the matrix to the screen
void Matrix::printMatrix() const
{
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      std::cout << "[" << matrix[i][j] << "]";
    }
    std::cout << std::endl;
  }
}

//addition operator to add two matrices
Matrix Matrix::operator+(const Matrix &A) const
{
  if(n_rows != A.getRows() or n_cols != A.getCols())
  {
    std::cout << "Error: The two matrices must be the same size" << std::endl;
  }
  else
  {
    Matrix B = Matrix(A.getRows(), A.getCols(), 0.0);
    for(int i=0; i<n_rows; i++)
    {
      for(int j=0; j<n_cols; j++)
      {
        B.setElement(i, j, matrix[i][j] + A.getElement(i,j));
      }
    }
    return B;
  }
}

//addition operator to add a scalar to all elements of a matrix
Matrix Matrix::operator+(const double &a) const
{
  Matrix B = Matrix(n_rows, n_cols, 0.0);
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      B.setElement(i, j, matrix[i][j] + a);
    }
  }
  return B;
}

//subtraction operator to subtract two matrices
Matrix Matrix::operator-(const Matrix &A) const
{
  if(n_rows != A.getRows() or n_cols != A.getCols())
  {
    std::cout << "Error: The two matrices must be the same size" << std::endl;
  }
  else
  {
    Matrix B = Matrix(A.getRows(), A.getCols(), 0.0);
    for(int i=0; i<n_rows; i++)
    {
      for(int j=0; j<n_cols; j++)
      {
        B.setElement(i, j, matrix[i][j] - A.getElement(i,j));
      }
    }
    return B;
  }
}

//subtraction operator to subtract a scalar from all elements of a matrix
Matrix Matrix::operator-(const double &a) const
{
  Matrix B = Matrix(n_rows, n_cols, 0.0);
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      B.setElement(i, j, matrix[i][j] - a);
    }
  }
  return B;
}

//mulitplication operator to multiply two matrices
Matrix Matrix::operator*(const Matrix &A) const
{
  if(n_rows != A.getRows() or n_cols != A.getCols())
  {
    std::cout << "Error: The two matrices must be the same size" << std::endl;
  }
  else
  {
    Matrix B = Matrix(A.getRows(), A.getCols(), 0.0);
    for(int i=0; i<n_rows; i++)
    {
      for(int j=0; j<n_cols; j++)
      {
        double tmp = 0;
        for(int k=0; k<n_cols; k++)
        {
          tmp += matrix[i][k]*A.getElement(k, j);
        }
        B.setElement(i, j, tmp);
      }
    }
    return B;
  }
}

//multiplication operator to multiply a scalar to all elements of a matrix
Matrix Matrix::operator*(const double &a) const
{
  Matrix B = Matrix(n_rows, n_cols, 0.0);
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      B.setElement(i, j, matrix[i][j] * a);
    }
  }
  return B;
}

//division operator to divide a scalar from all elements of a matrix
Matrix Matrix::operator/(const double &a) const
{
  Matrix B = Matrix(n_rows, n_cols, 0.0);
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      B.setElement(i, j, matrix[i][j] / a);
    }
  }
  return B;
}

//this transpose method swaps the rows and columns of a matrix
Matrix Matrix::transpose()
{
  Matrix B = Matrix(n_rows, n_cols, 0.0);
  for(int i=0; i<n_rows; i++)
  {
    for(int j=0; j<n_cols; j++)
    {
      B.setElement(i, j, matrix[j][i]);
    }
  }
  return B;
}
