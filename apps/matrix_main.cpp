#include "matrix.h"
//#include "matrix.cc"
#include <iostream>
#include <cstdlib>
#include <vector>

int main()
{
  std::cout << "Testing matrix default constructor..." << std::endl;
  std::cout << "Matrix A: " << std::endl;
  Matrix A = Matrix();
  A.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix initialization..." << std::endl;
  Matrix B = Matrix(3,4,2);
  std::cout << "Matrix B: " << std::endl;
  B.printMatrix();
  std::cout << std::endl;
  std::cout << "Testing getting number of rows and columns... " << std::endl;
  int rows_B, cols_B;
  rows_B = B.getRows();
  cols_B = B.getCols();
  std::cout << "Number of rows in matrix B: " << rows_B << std::endl;
  std::cout << "Number of columns in matrix B: " << cols_B << std::endl;
  std::cout << std::endl;

  std::cout << "Testing matrix copy constructor..." << std::endl;
  std::cout << "Matrix C = B" << std::endl;
  Matrix C = Matrix(B);
  C.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing set and get individual elements... " << std::endl;
  C.setElement(2,2,4);
  C.printMatrix();
  double c22;
  c22 = C.getElement(2,2);
  std::cout << "Element [2,2] of matrix C: " << c22 << std::endl;
  std::cout << std::endl;

  std::cout << "Testing set matrix method... " << std::endl;
  C.set(8);
  std::cout << "New Matrix C: " << std::endl;
  C.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing random set method... " << std::endl;
  C.setRandom();
  std::cout << "New Matrix C: " << std::endl;
  C.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix addition... " << std::endl;
  std::cout << "Matrix D: " << std::endl;
  Matrix D = Matrix(2,2,4.0);
  D.printMatrix();
  std::cout << "Matrix E: " << std::endl;
  Matrix E = Matrix(2,2,1.0);
  E.printMatrix();
  std::cout << "Matrix F = D + E" << std::endl;
  Matrix F = D + E;
  F.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix scalar addition... " << std::endl;
  std::cout << "Matrix G = E + 2" << std::endl;
  Matrix G = E + 2;
  G.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix subtraction... " << std::endl;
  std::cout << "Matrix H = D - E" << std::endl;
  Matrix H = D - E;
  H.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix scalar subtraction... " << std::endl;
  std::cout << "Matrix I = D - 2" << std::endl;
  Matrix I = D - 2;
  I.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix scalar multiplication... " << std::endl;
  std::cout << "Matrix J = D * 3" << std::endl;
  Matrix J = D * 3;
  J.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix multiplication... " << std::endl;
  Matrix M = Matrix(2,2,1.0);
  M.setElement(0,1,2.0);
  M.setElement(1,0,3.0);
  M.setElement(1,1,4.0);
  std::cout << "Matrix M: " << std::endl;
  M.printMatrix();
  Matrix N = Matrix(2,2,1.0);
  std::cout << "Matrix N: " << std::endl;
  N.printMatrix();
  Matrix L = M * N;
  std::cout << "Matrix L = M * N" << std::endl;
  L.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix scalar division... " << std::endl;
  std::cout << "Matrix K = J / 2" << std::endl;
  Matrix K = J / 2.0;
  K.printMatrix();
  std::cout << std::endl;

  std::cout << "Testing matrix transpose... " << std::endl;
  std::cout << "Matrix O = Transpose(M)" << std::endl;
  Matrix O = M.transpose();
  O.printMatrix();

  return 0;
}
