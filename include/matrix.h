#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <vector>

class Matrix {
  public:
    //constructors and destructor
    Matrix();
    Matrix(const Matrix &);
    Matrix(int, int, double);
    ~Matrix();

    //access and set methods
    int getRows() const;
    int getCols() const;
    double getElement(const int &, const int &) const;
    void set(const double &);
    void setRandom();
    void setElement(const int &, const int &, const double &);

    //print method
    void printMatrix() const;

    //matrix operations
    Matrix operator+(const Matrix &) const;
    Matrix operator+(const double &) const;
    Matrix operator-(const Matrix &) const;
    Matrix operator-(const double &) const;
    Matrix operator*(const Matrix &) const;
    Matrix operator*(const double &) const;
    Matrix operator/(const double &) const;
    Matrix transpose();

  private:
    //variables for the number of rows and columns of the matrix
    int n_rows, n_cols;
    //the matrix is a vector of vectors
    std::vector<std::vector<double> > matrix;
};

#endif
